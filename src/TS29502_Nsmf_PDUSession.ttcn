/******************************************************************************
* Copyright (c) 2000-2019  Ericsson AB
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v2.0
* which accompanies this distribution, and is available at
* https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
*
* Contributors:
*   Gabor Szalai - initial implementation and initial documentation
******************************************************************************/
//
//  File:               TS29502_Nsmf_PDUSession.ttcn
//  Description:	      Type definitions for 3GPP TS29502
/////////////////////////////////////////////// 
module TS29502_Nsmf_PDUSession {

  import from TS29571_CommonData all

  external function f_enc_PduSessionCreateError(in PduSessionCreateError pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_PduSessionCreateError(in octetstring stream, out PduSessionCreateError pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_HsmfUpdateError(in HsmfUpdateError pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_HsmfUpdateError(in octetstring stream, out HsmfUpdateError pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_VsmfUpdateError(in VsmfUpdateError pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_VsmfUpdateError(in octetstring stream, out VsmfUpdateError pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextCreatedData(in SmContextCreatedData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextCreatedData(in octetstring stream, out SmContextCreatedData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextCreateError(in SmContextCreateError pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextCreateError(in octetstring stream, out SmContextCreateError pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextStatusNotification(in SmContextStatusNotification pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextStatusNotification(in octetstring stream, out SmContextStatusNotification pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextRetrieveData(in SmContextRetrieveData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextRetrieveData(in octetstring stream, out SmContextRetrieveData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextRetrievedData(in SmContextRetrievedData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextRetrievedData(in octetstring stream, out SmContextRetrievedData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextUpdateData(in SmContextUpdateData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextUpdateData(in octetstring stream, out SmContextUpdateData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextUpdatedData(in SmContextUpdatedData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextUpdatedData(in octetstring stream, out SmContextUpdatedData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextUpdateError(in SmContextUpdateError pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextUpdateError(in octetstring stream, out SmContextUpdateError pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_SmContextReleaseData(in SmContextReleaseData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_SmContextReleaseData(in octetstring stream, out SmContextReleaseData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_PduSessionCreateData(in PduSessionCreateData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_PduSessionCreateData(in octetstring stream, out PduSessionCreateData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_PduSessionCreatedData(in PduSessionCreatedData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_PduSessionCreatedData(in octetstring stream, out PduSessionCreatedData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_StatusNotification(in StatusNotification pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_StatusNotification(in octetstring stream, out StatusNotification pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_VsmfUpdateData(in VsmfUpdateData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_VsmfUpdateData(in octetstring stream, out VsmfUpdateData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_VsmfUpdatedData(in VsmfUpdatedData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_VsmfUpdatedData(in octetstring stream, out VsmfUpdatedData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_HsmfUpdateData(in HsmfUpdateData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_HsmfUpdateData(in octetstring stream, out HsmfUpdateData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_HsmfUpdatedData(in HsmfUpdatedData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_HsmfUpdatedData(in octetstring stream, out HsmfUpdatedData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }

  external function f_enc_ReleaseData(in ReleaseData pdu) return octetstring 
  with { extension "prototype(convert) encode(JSON)" }

  external function f_dec_ReleaseData(in octetstring stream, out ReleaseData pdu) return integer 
  with { extension "prototype(backtrack) decode(JSON)" }


  type set SmContextCreateData {
    TS29571_CommonData.Supi    supi optional,
    boolean unauthenticatedSupi optional,
    TS29571_CommonData.Pei    pei optional,
    TS29571_CommonData.Gpsi    gpsi optional,
    TS29571_CommonData.PduSessionId    pduSessionId optional,
    TS29571_CommonData.Dnn    dnn optional,
    TS29571_CommonData.Snssai    sNssai optional,
    TS29571_CommonData.Snssai    hplmnSnssai optional,
    TS29571_CommonData.NfInstanceId    servingNfId,
    TS29571_CommonData.Guami    guami optional,
    charstring serviceName optional,
    TS29571_CommonData.PlmnId    servingNetwork,
    RequestType    requestType optional,
    TS29571_CommonData.RefToBinaryData    n1SmMsg optional,
    TS29571_CommonData.AccessType    anType,
    TS29571_CommonData.RatType    ratType optional,
    TS29571_CommonData.PresenceState    presenceInLadn optional,
    TS29571_CommonData.UserLocation    ueLocation optional,
    TS29571_CommonData.TimeZone    ueTimeZone optional,
    TS29571_CommonData.UserLocation    addUeLocation optional,
    TS29571_CommonData.Uri    smContextStatusUri,
    TS29571_CommonData.Uri    hSmfUri optional,
    set of       TS29571_CommonData.Uri    additionalHsmfUri optional,
    TS29571_CommonData.PduSessionId    oldPduSessionId optional,
    set of       TS29571_CommonData.PduSessionId    pduSessionsActivateList optional,
    EpsPdnCnxContainer    ueEpsPdnConnection optional,
    HoState    hoState optional,
    TS29571_CommonData.NfInstanceId    pcfId optional,
    TS29571_CommonData.Uri    nrfUri optional,
    TS29571_CommonData.SupportedFeatures    supportedFeatures optional,
    DnnSelectionMode    selMode optional,
    set of       TS29571_CommonData.BackupAmfInfo    backupAmfInfo optional,
    TS29571_CommonData.TraceData    traceData optional,
    EpsInterworkingIndication    epsInterworkingInd optional
  }

  type set SmContextCreatedData {
    TS29571_CommonData.Uri    hSmfUri optional,
    TS29571_CommonData.PduSessionId    pduSessionId optional,
    TS29571_CommonData.Snssai    sNssai optional,
    UpCnxState    upCnxState optional,
    TS29571_CommonData.RefToBinaryData    n2SmInfo optional,
    N2SmInfoType    n2SmInfoType optional,
    set of       EbiArpMapping    allocatedEbiList optional,
    HoState    hoState optional,
    TS29571_CommonData.SupportedFeatures    supportedFeatures optional
  }

  type set SmContextUpdateData {
    TS29571_CommonData.Pei    pei optional,
    TS29571_CommonData.Gpsi    gpsi optional,
    TS29571_CommonData.NfInstanceId    servingNfId optional,
    TS29571_CommonData.Guami    guami optional,
    TS29571_CommonData.PlmnId    servingNetwork optional,
    set of       TS29571_CommonData.BackupAmfInfo    backupAmfInfo optional,
    TS29571_CommonData.AccessType    anType optional,
    TS29571_CommonData.RatType    ratType optional,
    TS29571_CommonData.PresenceState    presenceInLadn optional,
    TS29571_CommonData.UserLocation    ueLocation optional,
    TS29571_CommonData.TimeZone    ueTimeZone optional,
    TS29571_CommonData.UserLocation    addUeLocation optional,
    UpCnxState    upCnxState optional,
    HoState    hoState optional,
    boolean toBeSwitched optional,
    boolean failedToBeSwitched optional,
    TS29571_CommonData.RefToBinaryData    n1SmMsg optional,
    TS29571_CommonData.RefToBinaryData    n2SmInfo optional,
    N2SmInfoType    n2SmInfoType optional,
    TS29571_CommonData.NfInstanceId    targetServingNfId optional,
    TS29571_CommonData.Uri    smContextStatusUri optional,
    boolean dataForwarding optional,
    set of       EpsBearerContainer    epsBearerSetup optional,
    set of       EpsBearerId    revokeEbiList optional,
    boolean release optional,
    Cause    cause optional,
    TS29571_CommonData.NgApCause    ngApCause optional,
    TS29571_CommonData.FiveGMmCause    fiveGMmCauseValue optional,
    TS29571_CommonData.Snssai    sNssai optional,
    TS29571_CommonData.TraceData    traceData optional,
    EpsInterworkingIndication    epsInterworkingInd optional
  } with {
    variant (fiveGMmCauseValue) "name as '5GMmCauseValue'"
  }

  type set SmContextUpdatedData {
    UpCnxState    upCnxState optional,
    HoState    hoState optional,
    set of       EpsBearerId    releaseEbiList optional,
    set of       EbiArpMapping    allocatedEbiList optional,
    set of       EbiArpMapping    modifiedEbiList optional,
    TS29571_CommonData.RefToBinaryData    n1SmMsg optional,
    TS29571_CommonData.RefToBinaryData    n2SmInfo optional,
    N2SmInfoType    n2SmInfoType optional,
    set of       EpsBearerContainer    epsBearerSetup optional,
    boolean dataForwarding optional
  }

  type set SmContextReleaseData {
    Cause    cause optional,
    TS29571_CommonData.NgApCause    ngApCause optional,
    TS29571_CommonData.FiveGMmCause    fiveGMmCauseValue optional,
    TS29571_CommonData.UserLocation    ueLocation optional,
    TS29571_CommonData.TimeZone    ueTimeZone optional,
    TS29571_CommonData.UserLocation    addUeLocation optional,
    boolean vsmfReleaseOnly optional
  } with {
    variant (fiveGMmCauseValue) "name as '5GMmCauseValue'"
  }


  type set SmContextStatusNotification {
    StatusInfo    statusInfo
  }

  type set PduSessionCreateData {
    TS29571_CommonData.Supi    supi optional,
    boolean unauthenticatedSupi optional,
    TS29571_CommonData.Pei    pei optional,
    TS29571_CommonData.PduSessionId    pduSessionId optional,
    TS29571_CommonData.Dnn    dnn,
    TS29571_CommonData.Snssai    sNssai optional,
    TS29571_CommonData.NfInstanceId    vsmfId,
    TS29571_CommonData.PlmnId    servingNetwork,
    RequestType    requestType optional,
    set of       EpsBearerId    epsBearerId optional,
    TS29571_CommonData.Bytes    pgwS8cFteid optional,
    TS29571_CommonData.Uri    vsmfPduSessionUri,
    TunnelInfo    vcnTunnelInfo,
    TS29571_CommonData.AccessType    anType,
    TS29571_CommonData.RatType    ratType optional,
    TS29571_CommonData.UserLocation    ueLocation optional,
    TS29571_CommonData.TimeZone    ueTimeZone optional,
    TS29571_CommonData.UserLocation    addUeLocation optional,
    TS29571_CommonData.Gpsi    gpsi optional,
    TS29571_CommonData.RefToBinaryData    n1SmInfoFromUe optional,
    TS29571_CommonData.RefToBinaryData    unknownN1SmInfo optional,
    TS29571_CommonData.SupportedFeatures    supportedFeatures optional,
    TS29571_CommonData.NfInstanceId    hPcfId optional,
    boolean hoPreparationIndication optional,
    DnnSelectionMode    selMode optional,
    EpsInterworkingIndication    epsInterworkingInd optional,
    charstring vSmfServiceInstanceId optional
  }

  type set PduSessionCreatedData {
    TS29571_CommonData.PduSessionType    pduSessionType,
    charstring sscMode,
    TunnelInfo    hcnTunnelInfo,
    TS29571_CommonData.Ambr    sessionAmbr,
    set of       QosFlowSetupItem    qosFlowsSetupList,
    TS29571_CommonData.PduSessionId    pduSessionId optional,
    TS29571_CommonData.Snssai    sNssai optional,
    boolean enablePauseCharging optional,
    TS29571_CommonData.Ipv4Addr    ueIpv4Address optional,
    TS29571_CommonData.Ipv6Prefix    ueIpv6Prefix optional,
    TS29571_CommonData.RefToBinaryData    n1SmInfoToUe optional,
    EpsPdnCnxInfo    epsPdnCnxInfo optional,
    set of       EpsBearerInfo    epsBearerInfo optional,
    TS29571_CommonData.SupportedFeatures    supportedFeatures optional,
    TS29571_CommonData.UpSecurity    upSecurity optional
  }

  type set HsmfUpdateData {
    RequestIndication    requestIndication,
    TS29571_CommonData.Pei    pei optional,
    TunnelInfo    vcnTunnelInfo optional,
    TS29571_CommonData.PlmnId    servingNetwork optional,
    TS29571_CommonData.AccessType    anType optional,
    TS29571_CommonData.RatType    ratType optional,
    TS29571_CommonData.UserLocation    ueLocation optional,
    TS29571_CommonData.TimeZone    ueTimeZone optional,
    TS29571_CommonData.UserLocation    addUeLocation optional,
    boolean pauseCharging optional,
    ProcedureTransactionId    pti optional,
    TS29571_CommonData.RefToBinaryData    n1SmInfoFromUe optional,
    TS29571_CommonData.RefToBinaryData    unknownN1SmInfo optional,
    set of       QosFlowItem    qosFlowsRelNotifyList optional,
    set of       QosFlowNotifyItem    qosFlowsNotifyList optional,
    set of       PduSessionNotifyItem    NotifyList optional,
    set of       EpsBearerId    epsBearerId optional,
    boolean hoPreparationIndication optional,
    set of       EpsBearerId    revokeEbiList optional,
    Cause    cause optional,
    TS29571_CommonData.NgApCause    ngApCause optional,
    TS29571_CommonData.FiveGMmCause    fiveGMmCauseValue optional,
    EpsInterworkingIndication    epsInterworkingInd optional
  } with {
    variant (fiveGMmCauseValue) "name as '5GMmCauseValue'"
  }


  type set HsmfUpdatedData {
    TS29571_CommonData.RefToBinaryData    n1SmInfoToUe optional
  }

  type set ReleaseData {
    Cause    cause optional,
    TS29571_CommonData.NgApCause    ngApCause optional,
    TS29571_CommonData.FiveGMmCause    fiveGMmCauseValue optional,
    TS29571_CommonData.UserLocation    ueLocation optional,
    TS29571_CommonData.TimeZone    ueTimeZone optional,
    TS29571_CommonData.UserLocation    addUeLocation optional
  } with {
    variant (fiveGMmCauseValue) "name as '5GMmCauseValue'"
  }


  //type object2 ReleasedData

  type set VsmfUpdateData {
    RequestIndication    requestIndication,
    TS29571_CommonData.Ambr    sessionAmbr optional,
    set of       QosFlowAddModifyRequestItem    qosFlowsAddModRequestList optional,
    set of       QosFlowReleaseRequestItem    qosFlowsRelRequestList optional,
    set of       EpsBearerInfo    epsBearerInfo optional,
    set of       EpsBearerId    assignEbiList optional,
    set of       EpsBearerId    revokeEbiList optional,
    set of       EbiArpMapping    modifiedEbiList optional,
    ProcedureTransactionId    pti optional,
    TS29571_CommonData.RefToBinaryData    n1SmInfoToUe optional,
    Cause    cause optional
  }

  type set VsmfUpdatedData {
    set of       QosFlowItem    qosFlowsAddModList optional,
    set of       QosFlowItem    qosFlowsRelList optional,
    set of       QosFlowItem    qosFlowsFailedtoAddModList optional,
    set of       QosFlowItem    qosFlowsFailedtoRelList optional,
    TS29571_CommonData.RefToBinaryData    n1SmInfoFromUe optional,
    TS29571_CommonData.RefToBinaryData    unknownN1SmInfo optional,
    TS29571_CommonData.UserLocation    ueLocation optional,
    TS29571_CommonData.TimeZone    ueTimeZone optional,
    TS29571_CommonData.UserLocation    addUeLocation optional,
    set of       EbiArpMapping    assignedEbiList optional,
    set of       EpsBearerId    failedToAssignEbiList optional,
    set of       EpsBearerId    releasedEbiList optional
  }

  type set StatusNotification {
    StatusInfo    statusInfo
  }

  type set QosFlowItem {
    TS29571_CommonData.Qfi    qfi,
    Cause    cause optional
  }

  type set QosFlowSetupItem {
    TS29571_CommonData.Qfi    qfi,
    TS29571_CommonData.Bytes    qosRules,
    TS29571_CommonData.Bytes    qosFlowDescription optional,
    QosFlowProfile    qosFlowProfile optional
  }

  type set QosFlowAddModifyRequestItem {
    TS29571_CommonData.Qfi    qfi,
    TS29571_CommonData.Bytes    qosRules optional,
    TS29571_CommonData.Bytes    qosFlowDescription optional,
    QosFlowProfile    qosFlowProfile optional
  }

  type set QosFlowReleaseRequestItem {
    TS29571_CommonData.Qfi    qfi,
    TS29571_CommonData.Bytes    qosRules optional,
    TS29571_CommonData.Bytes    qosFlowDescription optional
  }

  type set QosFlowProfile {
    TS29571_CommonData.FiveQi    fiveqi,
    TS29571_CommonData.NonDynamic5Qi    nonDynamic5Qi optional,
    TS29571_CommonData.Dynamic5Qi    dynamic5Qi optional,
    TS29571_CommonData.Arp    arp optional,
    GbrQosFlowInformation    gbrQosFlowInfo optional,
    TS29571_CommonData.ReflectiveQoSAttribute    rqa optional
  } with {
    variant (fiveqi) "name as '5qi'"
  }


  type set GbrQosFlowInformation {
    TS29571_CommonData.BitRate    maxFbrDl,
    TS29571_CommonData.BitRate    maxFbrUl,
    TS29571_CommonData.BitRate    guaFbrDl,
    TS29571_CommonData.BitRate    guaFbrUl,
    TS29571_CommonData.NotificationControl    notifControl optional,
    TS29571_CommonData.PacketLossRate    maxPacketLossRateDl optional,
    TS29571_CommonData.PacketLossRate    maxPacketLossRateUl optional
  }

  type set QosFlowNotifyItem {
    TS29571_CommonData.Qfi    qfi,
    NotificationCause    notificationCause
  }

  type set SmContextRetrieveData {
    MmeCapabilities    targetMmeCap optional
  }

  type set SmContextRetrievedData {
    EpsPdnCnxContainer    ueEpsPdnConnection
  }

  type set MmeCapabilities {
    boolean nonIpSupported optional
  }

  type set TunnelInfo {
    TS29571_CommonData.Ipv4Addr    ipv4Addr optional,
    TS29571_CommonData.Ipv6Addr    ipv6Addr optional,
    Teid    gtpTeid
  }

  type set StatusInfo {
    ResourceStatus    resourceStatus,
    Cause    cause optional
  }

  type set EpsPdnCnxInfo {
    TS29571_CommonData.Bytes    pgwS8cFteid,
    TS29571_CommonData.Bytes    pgwNodeName optional
  }

  type set EpsBearerInfo {
    EpsBearerId    ebi,
    TS29571_CommonData.Bytes    pgwS8uFteid,
    TS29571_CommonData.Bytes    bearerLevelQoS
  }

  type set PduSessionNotifyItem {
    NotificationCause    notificationCause
  }

  type set EbiArpMapping {
    EpsBearerId    epsBearerId,
    TS29571_CommonData.Arp    arp
  }

  type set SmContextCreateError {
    TS29571_CommonData.ProblemDetails    error_,
    TS29571_CommonData.RefToBinaryData    n1SmMsg optional
  } with {
    variant (error_) "name as 'error'"
  }


  type set SmContextUpdateError {
    TS29571_CommonData.ProblemDetails    error_,
    TS29571_CommonData.RefToBinaryData    n1SmMsg optional,
    TS29571_CommonData.RefToBinaryData    n2SmInfo optional,
    N2SmInfoType    n2SmInfoType optional,
    UpCnxState    upCnxState optional
  } with {
    variant (error_) "name as 'error'"
  }


  type set PduSessionCreateError {
    TS29571_CommonData.ProblemDetails    error_,
    charstring n1smCause optional,
    TS29571_CommonData.RefToBinaryData    n1SmInfoToUe optional
  } with {
    variant (error_) "name as 'error'"
  }


  type set HsmfUpdateError {
    TS29571_CommonData.ProblemDetails    error_,
    ProcedureTransactionId    pti optional,
    charstring n1smCause optional,
    TS29571_CommonData.RefToBinaryData    n1SmInfoToUe optional
  } with {
    variant (error_) "name as 'error'"
  }


  type set VsmfUpdateError {
    TS29571_CommonData.ProblemDetails    error_,
    ProcedureTransactionId    pti optional,
    charstring n1smCause optional,
    TS29571_CommonData.RefToBinaryData    n1SmInfoFromUe optional,
    TS29571_CommonData.RefToBinaryData    unknownN1SmInfo optional,
    set of       EpsBearerId    failedToAssignEbiList optional,
    TS29571_CommonData.NgApCause    ngApCause optional,
    TS29571_CommonData.FiveGMmCause    fiveGMmCauseValue optional
  } with {
    variant (error_) "name as 'error'"
    variant (fiveGMmCauseValue) "name as '5GMmCauseValue'"
  }


  type integer ProcedureTransactionId (0..255)

  type TS29571_CommonData.Uinteger    EpsBearerId

  type charstring EpsPdnCnxContainer

  type charstring EpsBearerContainer

  type charstring Teid // (pattern "^[A-F0-9]{8}$")

  type enumerated UpCnxState_enum { ACTIVATED, DEACTIVATED, ACTIVATING}

  type union UpCnxState {
    UpCnxState_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated HoState_enum { NONE, PREPARING, PREPARED, COMPLETED, CANCELLED}

  type union HoState {
    HoState_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated RequestType_enum { INITIAL_REQUEST, EXISTING_PDU_SESSION, INITIAL_EMERGENCY_REQUEST, EXISTING_EMERGENCY_PDU_SESSION}

  type union RequestType {
    RequestType_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated RequestIndication_enum { UE_REQ_PDU_SES_MOD, UE_REQ_PDU_SES_REL, PDU_SES_MOB, NW_REQ_PDU_SES_AUTH, NW_REQ_PDU_SES_MOD, NW_REQ_PDU_SES_REL, EBI_ASSIGNMENT_REQ}

  type union RequestIndication {
    RequestIndication_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated NotificationCause_enum { QOS_FULFILLED, QOS_NOT_FULFILLED, UP_SEC_FULFILLED, UP_SEC_NOT_FULFILLED}

  type union NotificationCause {
    NotificationCause_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated Cause_enum { REL_DUE_TO_HO, EPS_FALLBACK, REL_DUE_TO_UP_SEC, DNN_CONGESTION, S_NSSAI_CONGESTION, REL_DUE_TO_REACTIVATION, FIVEG_AN_NOT_RESPONDING, REL_DUE_TO_SLICE_NOT_AVAILABLE}
    with {
      variant "text 'FIVEG_AN_NOT_RESPONDING' as '5G_AN_NOT_RESPONDING'"
      variant "text 'S_NSSAI_CONGESTION' as 'S-NSSAI_CONGESTION'"
    }

  type union Cause {
    Cause_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated ResourceStatus_enum { RELEASED}

  type union ResourceStatus {
    ResourceStatus_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated DnnSelectionMode_enum { VERIFIED, UE_DNN_NOT_VERIFIED, NW_DNN_NOT_VERIFIED}

  type union DnnSelectionMode {
    DnnSelectionMode_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated EpsInterworkingIndication_enum { NONE, WITH_N26, WITHOUT_N26}

  type union EpsInterworkingIndication {
    EpsInterworkingIndication_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }


  type enumerated N2SmInfoType_enum { PDU_RES_SETUP_REQ, PDU_RES_SETUP_RSP, PDU_RES_SETUP_FAIL, PDU_RES_REL_CMD, PDU_RES_REL_RSP, PDU_RES_MOD_REQ, PDU_RES_MOD_RSP, PDU_RES_MOD_FAIL, PDU_RES_NTY, PDU_RES_NTY_REL, PDU_RES_MOD_IND, PDU_RES_MOD_CFM, PATH_SWITCH_REQ, PATH_SWITCH_SETUP_FAIL, PATH_SWITCH_REQ_ACK, PATH_SWITCH_REQ_FAIL, HANDOVER_REQUIRED, HANDOVER_CMD, HANDOVER_PREP_FAIL, HANDOVER_REQ_ACK, HANDOVER_RES_ALLOC_FAIL}

  type union N2SmInfoType {
    N2SmInfoType_enum  enum_val,
    charstring           other_val
  } with {
    variant "JSON: as value"
  }






} with {
  encode "JSON"
}
